globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('detail',{
            url:'/detail/{pid}',
            templateUrl: 'app/detail/views/detail.html',
            controller: 'detailCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/detail/ctrl/detailCtrl.js',
                    ]);
                }]
            }
        }) .
        state('detailmd',{
            url:'/detailmd/{pid}',
            templateUrl: 'app/detail/views/detailmd.html',
            controller: 'detailmdCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/detail/ctrl/detailmdCtrl.js',
                    ]);
                }]
            }
        })
        
});