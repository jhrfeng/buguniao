angular.module('app').controller('detailmdCtrl', 
['$scope', '$rootScope', 'http', '$state',
function($scope, $rootScope, http, $state) 
{
	$rootScope.header = false;
	$scope.comment = {pid:'', content:'', num:0, ok:true};
	$scope.vo = {number:1, showimg:''};

	function ngInit(){
		// $scope.detail = http.cacheUtil.getObject('detailmdPost');
		$scope.comment.pid = $state.params.pid;
		
		http.post(globalConfig.web + "/posts/sdetail", {pid:$state.params.pid}, function(data, status){
			$scope.detail = data;
			$scope.vo.showimg = $scope.detail.imgs[0];
		})

		http.get(globalConfig.web + "/home/source?source="+$state.params.pid, function(){});

		// 获取评论内容列表
		http.get(globalConfig.web+"/comment/query?type=1&pid="+$scope.comment.pid, function(data, status){
			$scope.commentList = data;
			$scope.comment.num = data.length;
		})
	}

	$scope.buy = function(){
		$state.go('m1order', {num:$scope.vo.number, size:angular.toJson({})})
	}

	ngInit();
	
}]);