angular.module('app').controller('downloadCtrl', 
['$scope', '$rootScope', 'http',
function($scope, $rootScope, http) 
{
	$rootScope.header = false;
	$rootScope.tab = '2';

	$scope.search = '1';
	$scope.list = [];
	$scope.comment = {orderid:'', pid:'', score:2, content:''};

	$scope.delete = function(orderid){
		$scope.comment.orderid = orderid;
	}

	$scope.deleteOk = function(){
		$('#canceld').modal('hide');
		http.get(globalConfig.web+'/order/cancelOrder?orderid='+$scope.comment.orderid, function(data, status){
			list();
		})
	}

	$scope.download = function(orderid, filename){
		// window.location.href = globalConfig.web+'/order/download?orderid='+orderid;
		http.get(globalConfig.web+'/order/download?orderid='+orderid, function(data, status){
			if(status==200)
				// window.location.href = globalConfig.web+'/order/getfile?key='+data.key;
				window.open(globalConfig.web+'/order/getfile?key='+data.key)
			else if(status==405)
				alert('正在为您更新源码，请1-3小时后尝试下载！')
			else
				alert('文件不存在或下载出错请联系网站客服')
		})
	}

	$scope.openModal = function(orderid, pid){
		$scope.comment.orderid = orderid;
		$scope.comment.pid = pid;
	}

	$scope.reply = function(){
		$('#myModal').modal('hide');
		http.post(globalConfig.web+'/comment/post', $scope.comment, function(data, status){
			if(status==200){
				alert('评价成功')
			}else if(status==405){
				alert('已经评价')
			}
		})
	}
	
	function list(){
		http.get(globalConfig.web+'/order/mylist', function(data, status){
			$scope.list = data;
		})
	}

	list();

}]);