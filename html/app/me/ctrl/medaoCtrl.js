angular.module('app').controller('medaoCtrl', 
['$scope', '$rootScope', 'http',
function($scope, $rootScope, http) 
{
	$rootScope.header = false;
	$rootScope.tab = '4';

	$scope.search = '1';
	$scope.list = [];
	$scope.comment = {orderid:'', pid:'', score:2, content:''};

	$scope.xuanze = function(orderid, pid){
		$scope.comment.orderid = orderid;
		$scope.comment.pid = pid;
	}

	$scope.deleteOk = function(){
		$('#cancel').modal('hide');
		http.get(globalConfig.web+'/order/cancelOrder?orderid='+$scope.comment.orderid, function(data, status){
			list()
		})
	}

	$scope.openModal = function(orderid, pid){
		$scope.comment.orderid = orderid;
		$scope.comment.pid = pid;
	}

	$scope.reply = function(){
		$('#hasok').modal('hide');
		http.post(globalConfig.web+'/comment/post', $scope.comment, function(data, status){
			if(status==405){
				alert('已经评价')
			}
		})
		http.post(globalConfig.web+'/order/hasOk', {orderid: $scope.comment.orderid}, function(data, status){
			if(status==200){
				list()
			}
		})
	}
	
	function list(){
		http.get(globalConfig.web+'/order/myslist', function(data, status){
			$scope.list = data;
		})
	}

	list();

}]);