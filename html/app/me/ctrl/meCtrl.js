angular.module('app').controller('meCtrl', 
['$scope', '$rootScope', 'http',
function($scope, $rootScope,http) 
{
	$rootScope.header = false;
	$rootScope.tab = '1';
	$rootScope.me = {email:'',grade:0,header:'',score:0,username:''}

	function ngInit(){
		http.get(globalConfig.web + "/user/me", function(data, status){
			if(status==200){
				$rootScope.me = data;
				http.cacheUtil.setObject("me", data);
			}
		})
	}

	ngInit()
}]);