angular.module('app').controller('settingCtrl', 
['$scope','$rootScope','http', 
function($scope,$rootScope,http) 
{
	$rootScope.header = false;
	$rootScope.tab = '1';
	$rootScope.me = {email:'',grade:0,header:'',score:0,username:''}
	$scope.user = {password:'*****', email:'', type:1};
	$scope.vo = {show:false, tip:false, msg:''};

	function ngInit(){
		http.get(globalConfig.web + "/user/me", function(data, status){
			if(status==200){
				$rootScope.me = data;
				http.cacheUtil.setObject("me", data);
				$scope.user.email = http.cacheUtil.getObject("me").email;
				$scope.user.address = http.cacheUtil.getObject("me").address;
			}
		})
	}

	ngInit()

	$scope.radio = function(index){
		if(index==0){
			$scope.user.address[0].type = true;
			$scope.user.address[1].type = false;
			$scope.user.address[2].type = false;
		}
		if(index==1){
			$scope.user.address[1].type = true;
			$scope.user.address[0].type = false;
			$scope.user.address[2].type = false;
		}
		if(index==2){
			$scope.user.address[2].type = true;
			$scope.user.address[1].type = false;
			$scope.user.address[0].type = false;
		}
	}

	$scope.setting = function(type){
		$scope.user.type = type;
		if(type==1){
			if($scope.user.password=='' || $scope.user.password=='*****'){
				$scope.vo.tip = true;
				$scope.vo.msg = '密码不能设置为空或重新设置';
				return false;
			}
		}

		if(type==2){
			if($scope.user.email=='' || $scope.user.email=='*****'){
				$scope.vo.tip = true;
				$scope.vo.msg = '邮箱不能设置为空或格式错误';
				return false;
			}
		}
		var reqUrl = globalConfig.web + "/user/setting";
		http.post(reqUrl, $scope.user, function(data, status){
			if(status==200){
				$scope.vo.tip = true;
				$scope.vo.msg = '设置成功';
				http.cacheUtil.setObject("me", $scope.user);
			}
		})
	}

}]);