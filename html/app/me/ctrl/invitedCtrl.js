angular.module('app').controller('invitedCtrl', 
['$scope', '$rootScope', 'http',
function($scope, $rootScope, http) 
{
	$rootScope.header = false;
	$rootScope.tab = '3';
	
	http.get(globalConfig.web+'/invite/list', function(data, status){
		$scope.list = data;
	})
	
}]);