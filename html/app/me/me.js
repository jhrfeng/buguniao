globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('me',{
            url:'/me',
            templateUrl: 'app/me/views/me.html',
            controller: 'meCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/me/ctrl/meCtrl.js',
                    ]);
                }]
            }
        }).state('me.setting',{
            url:'/setting',
            templateUrl: 'app/me/views/setting.html',
            controller: 'settingCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/me/ctrl/settingCtrl.js',
                    ]);
                }]
            }
        }).state('me.download',{
            url:'/download',
            templateUrl: 'app/me/views/download.html',
            controller: 'downloadCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/me/ctrl/downloadCtrl.js',
                    ]);
                }]
            }
        }).state('me.invited',{
            url:'/invited',
            templateUrl: 'app/me/views/invited.html',
            controller: 'invitedCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/me/ctrl/invitedCtrl.js',
                    ]);
                }]
            }
        }).state('me.medao',{
            url:'/medao',
            templateUrl: 'app/me/views/medao.html',
            controller: 'medaoCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/me/ctrl/medaoCtrl.js',
                    ]);
                }]
            }
        })
        
});