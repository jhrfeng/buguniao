angular.module('app').controller('homelistCtrl', 
['$scope', '$rootScope', 'stateV','http','$state',
function($scope, $rootScope,stateV,http,$state) 
{	
	$rootScope.header = false;	
	$scope.sorts = {time:'glyphicon-triangle-top', price:'glyphicon-triangle-top', num:'glyphicon-triangle-top'};  

	$scope.listsort = function(type){
		if(type=='created'){
			$scope.query.created = Number($scope.query.created)*-1 + '';
			$scope.sorts.time = $scope.sorts.time=='glyphicon-triangle-top'?'glyphicon-triangle-bottom':'glyphicon-triangle-top';
		}else if(type=='price'){
			$scope.query.price = Number($scope.query.price)*-1 + '';
			$scope.sorts.price = $scope.sorts.price=='glyphicon-triangle-top'?'glyphicon-triangle-bottom':'glyphicon-triangle-top';
		}else if(type=='times'){
			$scope.query.times = Number($scope.query.times)*-1 + '';
			$scope.sorts.num = $scope.sorts.num=='glyphicon-triangle-top'?'glyphicon-triangle-bottom':'glyphicon-triangle-top';
		}
		$scope.collect.search();
	}

	$scope.tag = function(type){
		if($scope.query.type[type] == type){
			delete $scope.query.type[type] //= undefined;
		}else{
			$scope.query.type[type] = type
		}
		$scope.collect.search();
	}

	$scope.query = {name:$state.params.name, created:'-1', price:'1', times:'1', type:{}, pages:0, total:0, totalPages:0};

	$scope.collect = {
		search: function(){
			$scope.query.pages = 0;
			list();
		},
		reset: function(){
			$scope.query.name = '';
			$scope.query.codes = '';
			$scope.query.type = '';
			$scope.query.hot = '';
		},
		currentPage:function(num){
			$scope.query.pages = $scope.collect.showPageVal(num);
			list();
		},
		prePage: function(){
			if($scope.query.pages >= 1){
				--$scope.query.pages
				list();
			}
		},
		nextPage: function(){
			if(($scope.query.pages+1) < $scope.query.totalPages){
				++$scope.query.pages
				list();
			}
		},
		showPage: function(num){
			if((switchPage($scope.query.pages) + num) < $scope.query.totalPages)
				return true
			else
				return false
		},
		showPageVal: function(num){
			return switchPage($scope.query.pages) + num;
		}
	};

	$scope.detail = function(data){
		// http.cacheUtil.setObject('detailPost', data);
		$state.go('detail', {pid:data.pid})
	}



	function list(){
		var reqUrl = globalConfig.web + '/posts/query';
		http.post(reqUrl, $scope.query, function(data, status){
			$scope.query.total = data.count;
			$scope.query.totalPages = totalPage($scope.query.total);
			$scope.postList = data.list; 
		})
	}

	list();
	
}]);