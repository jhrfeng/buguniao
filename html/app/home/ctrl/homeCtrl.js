angular.module('app').controller('homeCtrl', 
['$scope', '$rootScope', 'http', 'stateV', '$state', '$interval',
function($scope, $rootScope, http, stateV, $state, $interval) 
{	
	$rootScope.header = false;

	function ngInit(){
		$one_minute = $interval(function(){
			ShowCountDown(2017,9,1)
		},1000);
			
		http.get(globalConfig.web + "/home/source?source=home", function(){});

		http.get(globalConfig.web + '/posts/oneList', function(data, status){
			if(status==200) $scope.ones = data;
		})

		http.get(globalConfig.web + '/posts/hotList', function(data, status){
			if(status==200) $scope.hots = data;
		})

		http.get(globalConfig.web + '/posts/newList', function(data, status){
			if(status==200) $scope.news = data;
		})

		http.get(globalConfig.web + '/home/getting', function(data, status){
			if('null'!=data){
				$scope.setting = data;
			}
		})
	}

	$scope.detail = function(data){
		// stateV.set(data);
		// http.cacheUtil.setObject('detailPost', data);
		$state.go('detail', {pid:data.pid})
	}

	function ShowCountDown(year,month,day) 
	{ 
		var now = new Date(); 
		var endDate = new Date(year, month-1, day); 
		var leftTime=endDate.getTime()-now.getTime(); 
		var leftsecond = parseInt(leftTime/1000); 
		var day1=Math.floor(leftsecond/(60*60*24)); 
		var hour=Math.floor((leftsecond-day1*24*60*60)/3600); 
		var minute=Math.floor((leftsecond-day1*24*60*60-hour*3600)/60); 
		var second=Math.floor(leftsecond-day1*24*60*60-hour*3600-minute*60); 
		$scope.time = day1+"天 "+hour+"时 "+minute+"分 "+second+"秒"
	} 
	
	ngInit()
	
}]);