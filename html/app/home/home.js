globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('home',{
            url:'/home',
            templateUrl: 'app/home/views/home.html',
            controller: 'homeCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/home/ctrl/homeCtrl.js',
                    ]);
                }]
            }
        })
        .state('home_list',{
            url:'/home_list/{name}',
            templateUrl: 'app/home/views/list.html',
            controller: 'homelistCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/home/ctrl/listCtrl.js',
                    ]);
                }]
            }
        })
        .state('madao',{
            url:'/madao',
            templateUrl: 'app/home/views/madao.html',
            controller: 'madaoCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/home/ctrl/madaoCtrl.js',
                    ]);
                }]
            }
        })
        
});
