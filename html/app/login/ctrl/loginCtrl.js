angular.module('app').controller('loginCtrl', 
['$scope', '$rootScope','http', '$state', 
function($scope, $rootScope,http,$state) {
	$rootScope.header = true;
	$scope.vo = {height:(window.screen.height-130) , error:true, errmsg:''};

	$scope.login = function(){
		var reqUrl = globalConfig.web + "/user/signin";
		if(validate()){
            http.signin(reqUrl, $scope.user, function(data, status){
				if(status==200){
					http.cacheUtil.put("Authorization", data.token);
					http.cacheUtil.put("showMe", true);
					$rootScope.showMe = true;
					$state.go("me.setting")
				}else if(status==401){
					$scope.vo.error = false;
	        		$scope.vo.errmsg = "账号或密码错误"
				}
			});
		}
	}

	function validate(){
		if(!validatemobile($scope.user.username)){
			$scope.vo.error = false;
	    	$scope.vo.errmsg = '请输入有效的手机号!';
			return false;
		} 
		return true;
	}

	function validatemobile(newname) {
	    if (newname=='' || newname==null) {
	        return false;
	    }
	    if (newname.length != 11) {
	        return false;
	    }
	    var PATTERN_CHINAMOBILE = /^1(3[4-9]|5[0123789]|8[23478]|4[7]|7[8])\d{8}$/; //移动号
	    var PATTERN_CHINAUNICOM = /^1(3[0-2]|5[56]|8[56]|4[5]|7[6])\d{8}$/; //联通号
	    var PATTERN_CHINATELECOM = /^1(3[3])|1(7[0123])|(8[019])\d{8}$/; //电信号
	    if (PATTERN_CHINAUNICOM.test(newname)) {
	        return true;
	    } else if (PATTERN_CHINAMOBILE.test(newname)) {
	        return true;
	    } else if (PATTERN_CHINATELECOM.test(newname)) {
	        return true;
	    }else {
	        return false;
	    }
	}
	
}]);