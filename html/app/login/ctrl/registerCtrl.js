angular.module('app').controller('registerCtrl', 
['$scope', '$rootScope','http', '$state',
function($scope, $rootScope,http,$state) {
	$rootScope.header = true;
	$scope.vo = {height:(window.screen.height-130), error:true, errmsg:''};
	$scope.user = {getCode:'获取验证码', }

	function ngInit(){
		$scope.reSend();
	}

	$scope.register = function(){
		var reqUrl = globalConfig.web + "/user/register";
		if(validate()){
			if($scope.user.smscode=="" || null==$scope.user.smscode){
				$scope.vo.error = false;
	        	$scope.vo.errmsg = "请输入手机验证码"
				return false;
			}
            http.signin(reqUrl, $scope.user, function(data, status){
				if(status==200){
					http.cacheUtil.put("Authorization", data.token);
					http.cacheUtil.put("showMe", true);
					$rootScope.showMe = true;
					$state.go("me.setting")
				}else if(status==501){
					$scope.vo.error = false;
	        		$scope.vo.errmsg = "手机验证码不正确，请核实"
				}else if(status==503){
					$scope.vo.error = false;
	        		$scope.vo.errmsg = "邀请账号不能为注册账号"
				}
			});
		}
	}

	//短信验证码 
	$scope.sendSms = function(){
		$scope.vo.error = true;
		if(validate()){
			var reqUrl = globalConfig.web + "/validate/sendsms";
	        http.post(reqUrl, $scope.user, function(data, status){
	        	if(status==500 || status==402){
	        		$scope.vo.error = false;
	        		$scope.vo.errmsg = '问题校验答案不正确，请刷新';
	        		$scope.reSend();
	        		return;
	        	}
	        	if(status==200){
	        		countDown(60); // 倒计时
	        		if(data.status==501){
	        			$scope.vo.error = false;
	        			$scope.vo.errmsg = data.msg;
	        		}
	        	}
	        })
		}
	}

	// 动态校验码
	$scope.reSend = function(){
		$scope.vo.error = true;
		var reqUrl = globalConfig.web + "/validate/reSend";
		http.get(reqUrl,  function(data, status){
			if(status==200){
				$scope.user.vtext = data.que;
				$scope.user.vid = data.id;
			}
		});
	}

	function countDown(times){
		$('#sms').attr({"disabled":"disabled"});
    	if(!times||isNaN(parseInt(times)))return;
    	var args = arguments;
    	var self = this;
     	$('#sms').text(times+"s  ");
     	setTimeout(function(){
     		args.callee.call(self,--times);
     		if(times==0){
     			$('#sms').removeAttr("disabled");
     			$('#sms').text('点击发送');
     		}

     	},1000); 	
 	}

 	function validate(){
		if(!validatemobile($scope.user.username)){
			$scope.vo.error = false;
	    	$scope.vo.errmsg = '请输入有效的手机号!';
			return false;
		} 
		if($scope.user.vcode=="" || null==$scope.user.vcode){
			$scope.vo.error = false;
			$scope.vo.errmsg = '请输入动态验证码';
			return false;
		}
		return true;
	}

	function validatemobile(newname) {
	    if (newname=='' || newname==null) {
	        return false;
	    }
	    if (newname.length != 11) {
	        return false;
	    }
	    var PATTERN_CHINAMOBILE = /^1(3[4-9]|5[0123789]|8[23478]|4[7]|7[8])\d{8}$/; //移动号
	    var PATTERN_CHINAUNICOM = /^1(3[0-2]|5[56]|8[56]|4[5]|7[6])\d{8}$/; //联通号
	    var PATTERN_CHINATELECOM = /^1(3[3])|1(7[0123])|(8[019])\d{8}$/; //电信号
	    if (PATTERN_CHINAUNICOM.test(newname)) {
	        return true;
	    } else if (PATTERN_CHINAMOBILE.test(newname)) {
	        return true;
	    } else if (PATTERN_CHINATELECOM.test(newname)) {
	        return true;
	    }else {
	        return false;
	    }
	}

	ngInit();
}]);