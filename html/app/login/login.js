globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('login',{
            url:'/login',
            templateUrl: 'app/login/views/login.html',
            controller: 'loginCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/login/ctrl/loginCtrl.js',
                    ]);
                }]
            }
        })
        .state('register',{
            url:'/register',
            templateUrl: 'app/login/views/register.html',
            controller: 'registerCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/login/ctrl/registerCtrl.js',
                    ]);
                }]
            }
        })
});