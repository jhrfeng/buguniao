angular.module('app')
    .directive('backToTop', function() {
        return {  
            restrict: "E",  
            link: function (scope, element, attr) {  
                var e = $(element);  
                $(window).scroll(function () {                 //滚动时触发  
                    if ($(document).scrollTop() > 300)         //获取滚动条到顶部的垂直高度,到相对顶部300px高度显示  
                        e.fadeIn(300)  
                    else  
                        e.fadeOut(200);  
                });  
                /*点击回到顶部*/  
                // e.click(function () {  
                    $('html, body').animate({                 //添加animate动画效果  
                        scrollTop: 0  
                    }, 500);  
                // });  
            }  
        };  
})