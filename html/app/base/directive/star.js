angular.module('app')
    .directive('star', function() {
	    return {
	        priority:1000,
	        restrict: 'A',
	        scope: {
	            star: '=',
	        },
	        template: '<span class="glyphicon {{star>0?\'glyphicon-star\':\'glyphicon-star-empty\'}}"></span>\
	        		   <span class="glyphicon {{star>1?\'glyphicon-star\':\'glyphicon-star-empty\'}}"></span>\
	        		   <span class="glyphicon {{star>2?\'glyphicon-star\':\'glyphicon-star-empty\'}}"></span>\
	        		   <span class="glyphicon {{star>3?\'glyphicon-star\':\'glyphicon-star-empty\'}}"></span>\
	        		   <span class="glyphicon {{star>4?\'glyphicon-star\':\'glyphicon-star-empty\'}}"></span>'
	    };
})