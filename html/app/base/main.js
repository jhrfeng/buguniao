globalConfig = {
	// web:'http://127.0.0.1:3000',
	// web:'https://www.bgncode.com',
	// web:'http://cat-vip.vicp.io',
	web: 'http://www.mcp5188.cn',
	app: ''
};


(function(document) {

	globalConfig.app = angular.module('app', ['ui.router','oc.lazyLoad']);

	var routerList = [
		'app/home/home.js',
		'app/login/login.js',
		'app/detail/detail.js',
		'app/me/me.js',
		'app/order/order.js',
		'app/plat/plat.js',
	];
	
	var str = '';
	routerList.forEach(function(router) {
		str += '<script src="' + router + '"></script>';
	});
	document.write(str);

}(document));


angular.module('app').config(['$urlRouterProvider', function($urlRouterProvider) {
	$urlRouterProvider.when('', '/home');
}]);

angular.module('app').controller('rootCtrl',['$rootScope','http',function($rootScope, http){
	$rootScope.header = false;
	$rootScope.showMe = window.localStorage.getItem("showMe");

	$rootScope.logout = function(){
		http.get(globalConfig.web+'/user/logout', function(data, status){
			http.cacheUtil.remove("Authorization")
			http.cacheUtil.remove("showMe")
            $rootScope.showMe = false;
        })
	}


}]).factory('stateV', function() {
	var savedData = {}
 	function set(data) {
   		savedData = data;
 	}
	function get() {
  		return savedData;
 	}
 	return {
  		set: set,
  		get: get
 	}
});


(function(document) {
	var libList = [
		'app/base/other/filter.js',
		'app/base/other/httpUtil.js',
		'app/base/directive/star.js',
		'app/base/directive/backTop.js',
	];
	var str = '';
	libList.forEach(function(lib) {
		str += '<script src="' + lib + '"></script>';
	});
	document.write(str);
}(document));




