angular.module('app').filter('trustHtml', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
}).filter('grade', function ($sce) {
    return function (input) {
        if(input==0)
        	return "普通会员"
        else if(input==1)
        	return "白银会员"
        else if(input==2)
        	return "铂金会员"
        else if(input==3)
        	return "钻石会员"
        else if(input==4)
        	return "最强会员"
    }
}).filter('orderStatus', function ($sce) {
    return function (input) {
        if(input==0)
            return "等待支付"
        else if(input==1)
            return "支付成功，等待发货"
        else if(input==2)
            return "已发货"
        else if(input==3)
            return "确认收货"
        else if(input==4)
            return "已完成"
        else if(input==10)
            return "申请退款"
    }
});