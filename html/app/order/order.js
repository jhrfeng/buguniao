globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('order',{
            url:'/order/{orderid}',
            templateUrl: 'app/order/views/order.html',
            controller: 'orderCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/order/ctrl/orderCtrl.js',
                    ]);
                }]
            }
        }).state('return',{
            url:'/return',
            templateUrl: 'app/order/views/return.html',
            controller: 'returnCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/order/ctrl/returnCtrl.js',
                    ]);
                }]
            }
        }).state('m1order',{
            url:'/m1order/{num}/{size}',
            templateUrl: 'app/order/views/m1order.html',
            controller: 'm1orderCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/order/ctrl/m1orderCtrl.js',
                    ]);
                }]
            }
        }).state('m2order',{
            url:'/m2order/{orderid}',
            templateUrl: 'app/order/views/m2order.html',
            controller: 'm2orderCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/order/ctrl/m2orderCtrl.js',
                    ]);
                }]
            }
        });
        
});