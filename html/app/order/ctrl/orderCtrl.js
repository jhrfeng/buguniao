angular.module('app').controller('orderCtrl', 
['$scope', '$rootScope', 'stateV', 'http','$state',
function($scope, $rootScope,stateV,http,$state) 
{
	$rootScope.header = false;
	// $scope.detail = angular.fromJson($stateParams.order);
	http.get(globalConfig.web+"/order/detail?orderid="+$state.params.orderid,  function(data, status){
		if(status==200){
			$scope.detail = data;
		}
	})
	
	$scope.buy = function(){
		var payUrl = globalConfig.web + "/aplipay/pay";
		http.post(payUrl, {orderid:$state.params.orderid}, function(data, status){
			if(status==200){
				window.location.href=data.url;
			}else if(status==403){
				alert("订单信息错误，无法进行支付")
			}
		})
	}
}]);