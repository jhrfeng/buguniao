angular.module('app').controller('m1orderCtrl', 
['$scope', '$rootScope', 'stateV', 'http','$state',
function($scope, $rootScope,stateV,http,$state) 
{
	$rootScope.header = false;
	$scope.addrs = [];
	$scope.vo    = {btext:'保存到地址一', index:0, number:0, size:{}};
	$scope.vo.number = $state.params.num;
	$scope.vo.size   = angular.fromJson($state.params.size);
	console.log($scope.vo.size)
	function ngInit(){
		$scope.detail = http.cacheUtil.getObject('detailmdPost');
		getAddr()
	}

	$scope.setaddr = function(index){
		$scope.addr = $scope.addrs[index]
	}

	$scope.saddr = function(text, index){
		$scope.vo.index = index;
		$scope.vo.btext = text;
	}

	$scope.saveAddr = function(){
		var payUrl = globalConfig.web + "/user/setAddr";
		http.post(payUrl, {index:$scope.vo.index, addr:$scope.addr}, function(data, status){
			if(status==200){
				getAddr()
				alert('保存成功')
			}
		})
	}
	
	$scope.buy = function(){
		var payUrl = globalConfig.web + "/order/snew";
		http.post(payUrl, {pid:$scope.detail.pid, number:$scope.vo.number, addr:$scope.addr, size:$scope.vo.size}, function(data, status){
			if(status==200){
				$state.go('m2order', {orderid: data.orderid})
			}else if(status==403){
				alert("订单信息错误")
			}
		})
	}

	function getAddr(){
		http.get(globalConfig.web + "/user/me", function(data, status){
			$scope.addrs = data.address;
			angular.forEach($scope.addrs, function(addr, i){
				if(addr.type){
					$scope.addr  = addr;
					$scope.vo.index = i;
				}
			})
		})
	}

	ngInit()

}]);