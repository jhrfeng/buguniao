globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('platou',{
            url:'/platou',
            templateUrl: 'app/plat/views/platou.html',
            controller: 'platouCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/plat/ctrl/platouCtrl.js',
                    ]);
                }]
            }
        }).state('zhaopin',{
            url:'/zhaopin',
            templateUrl: 'app/plat/views/zhaopin.html',
            controller: 'zhaopinCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/plat/ctrl/zhaopinCtrl.js',
                    ]);
                }]
            }
        }).state('advice',{
            url:'/advice',
            templateUrl: 'app/plat/views/advice.html',
            controller: 'adviceCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/plat/ctrl/adviceCtrl.js',
                    ]);
                }]
            }
        }).state('bug',{
            url:'/bug',
            templateUrl: 'app/plat/views/bug.html',
            controller: 'bugCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/plat/ctrl/bugCtrl.js',
                    ]);
                }]
            }
        }).state('about',{
            url:'/about',
            templateUrl: 'app/plat/views/about.html'
        }).state('aproduct',{
            url:'/aproduct',
            templateUrl: 'app/plat/views/aproduct.html'
        });
        
});