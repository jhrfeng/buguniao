angular.module('app').controller('zhaopinCtrl', 
['$scope', '$rootScope', 'stateV', 'http','$state',
function($scope, $rootScope,stateV,http,$state) 
{
	$rootScope.header = false;

	$scope.list = [
		{
			title:"招聘MT4、CRM开发", 
			time:"2017-8-8", 
			show:false,
			content1:"薪资在8-14K公司是金融行业，主要做黄金外汇股票的，是上海金融大型互联网公司",
			content2:"1、负责MT4平台、CRM系统的迭代开发和正常运行维护  2、负责白标平台的搭建；", 
			content3:"任职资格：1、具有MT4、CRM开发、2年以上第三方存储资金管理支付接口经验的优先;2、有独立思考和判断能力，思维清晰敏捷，有自己的想法；",
			content4:"邮箱(简历标注职位)：215401886@gmail.com,   QQ：215401886",
			hide:"lhyb1826|330185192"
		},
		{
			title:"寻找一名IONIC3开发者", 
			time:"2017-7-27", 
			show:false,
			content1:"本人想做一款商业类APP，具体可以参考网易严选， 只需要做APP及对接接口调试。价格好商量，可以商谈。如果愿意者请联系我。", 
			content2:"邮箱(简历标注职位)：215401886@gmail.com,   QQ：215401886"
		}

	];

	
}]);