angular.module('app').controller('bugCtrl', 
['$scope', '$rootScope', 'stateV', 'http','$state',
function($scope, $rootScope,stateV,http,$state) 
{
	$rootScope.header = false;
	$scope.plat = {title:'', content:''}

	$scope.add = function(){
		if($scope.plat.title.trim()=='' || $scope.plat.content.trim()==''){
			alert("标题或内容不能为空")
			return false;
		}
		var url = globalConfig.web + "/plat/addbug";
		http.post(url, $scope.plat, function(data, status){
			if(status==200){
				alert("提交成功")
			}else if(status==403){
				alert("内容不能为空")
			}
		})
	}

}]);