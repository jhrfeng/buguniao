function totalPage(pages){
	return Math.ceil(pages/12)
}

function switchPage(pages){
	var newNum = String((pages+1)/5)   // 每页10条一共5列
	if(newNum.indexOf(".")>-1){
		newNum = newNum.substr(0, newNum.indexOf("."))
	}
	if(newNum > 0){
		return Number(newNum)*4
	}
	return Number(newNum)
}