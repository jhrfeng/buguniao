var mongoose = require('mongoose');
var md5 = require('md5');
var SALT_WORK_FACTOR = 10;
var mongodbURL = 'mongodb://127.0.0.1:27017/bgn';
var mongodbOptions = { };

mongoose.connect(mongodbURL, mongodbOptions, function (err, db) {
    if (err) { 
        console.log('Connection refused to ' + mongodbURL);
        console.log(err);
    } else {
        console.log('Connection successful to: ' + mongodbURL);
    }
});

var Schema = mongoose.Schema;

// User schema
var User = new Schema({
    username: { type: String, required: true, unique: true }, //手机号
    password: { type: String, required: true }, // 密码 md5
    header:   { type: String, default: "http://bgn.oss-cn-shanghai.aliyuncs.com/header/cat-01.png!small"}, //头像
    score:    { type: Number, default: 0}, //积分
    grade:    { type: Number, default: 0}, //等级1普通；2白银；3铂金；4超凡大师；5最强王者
    qq:       { type: String, default: ""}, //qq
    weixin:   { type: String, default: ""}, //weixin
    email:    { type: String, default: ""}, //邮箱
    mailSec:  { type: Boolean, default: false },  //邮箱认证，是否安全是自己的
    address:  { type: Array,  default: [{name:'', mobile:'', addr:'', zip:'',type:true}, {name:'', mobile:'', addr:'', zip:'',type:false}, {name:'', mobile:'', addr:'', zip:'',type:false}]}, //地址
    otherTel: { type: String, default: ""}, //其他联系人的联系方式
    remoteip: { type: Array             }, //登录者的ip
    is_admin: { type: Boolean, default: false }, // 账号类型
    userStatus: { type: String, default:"0"},  // 用户状态，是否认证， 0没有认证,1认证中，2已认证
    mobStatus:  { type: String, default:"0"}, // 手机状态, 0未认证
    roletype: { type: String, default: "0"}, // 用户类型，默认0； 1是业务员
    idcard:   { type: String, default: ""}, // 身份证号
    nick:     { type: String, default: ""}, // 昵称
    name:     { type: String, default: ""}, // 真实姓名
    other:    { type: Object                 },
    infomation:{ type: Object                 },
    status:   { type: String, default: "1" },
    updated:  { type: Date, default: Date.now},
    created:  { type: Date, default: Date.now }
});

// 奖励金系统
var Invite = new Schema({
    userid:   { type: String, required: true }, // 关联的用户[username]
    inviteid: { type: String, required: true }, // 关联的邀请人[username]
    type:     { type: String, default: "0" },   //  0 是普通用户，1是业务员 
    score:    { type: Number, default: 0}, //积分
    orderid:  { type: String, default: "" },    // 订单号
    tradeno:  { type: String, default: "" },    // 订单号
    pid:      { type: Object                 }, // 商品信息
    payAmount:{ type: Number  , default: 0  }, // 订单金额
    money:    { type: Number, default: 0 },
    status:   { type: String, default: "0" },    // 0注册，1有首单对方已支付，2提现
    updated:  { type: Date, default: Date.now },
    created:  { type: Date, default: Date.now }

})

// gold系统
var Gold = new Schema({
    g:        { type: String, default: "0" },   // 克数
    value:    { type: Number, required: true }, // 设置阈值
    status:   { type: String, default: "0" },   // 是否通知
    date:     { type: String, default: "0" },   // 时间  
    updated:  { type: Date, default: Date.now },
    created:  { type: Date, default: Date.now }
})

// 商品
var Post = new Schema({
    pid:     { type: String, required: true, unique: true }, // 商品id
    name:    { type: String, required: false}, // 商品名称
    hot:     { type: String, default: "0"   },     // 0一般；1热门，2 最新
    codes:   { type: String, unique: true   }, // 商品编号
    oldprice:{ type: Number, default: 0     }, // 旧价格
    price:   { type: Number, default: 0     }, // 价格
    yprice:  { type: Number, default: 0     }, // 运送费
    times:   { type: Number, default: 0     }, // 下载次数
    shcema:  { type: Number, default: 100   }, // 库存数量
    size:    { type: Object                 }, // 规格
    issize:  { type: String, default:'0'    }, // 是否显示规格
    start:   { type: Object, default: {'a':2, 'b':1}}, // {总分，总次数}
    tag:     { type: String, default: ""    }, // 标签
    tagimg:  { type: String, default: ""    }, // 标签图片
    type:    { type: String, default: ""    }, // 大类 1源码；2书籍；3商品
    type1:    { type: String, default: ""   }, // 小类 1源码；2书籍；3商品
    addr:    { type: String, default: ""    }, // 源码
    title:   { type: String, default: ""    }, // 商品简介
    imgs:    { type: Array                  }, // 简介图片
    codeyuan:{ type: String, default: ""    }, // 标注购买出处;账号密码
    version: { type: String, default: ""    }, // 商品版本
    demo:    { type: String, default: ""    }, // DEMO演示地址
    download:{ type: String, default: ""    }, // 商品后台下载地址
    credate: { type: String, default: ""    }, // 商品建立日期
    modate:  { type: String, default: ""    }, // 商品修改日期
    postlog: { type: String, default: ""    }, // 商品更新日志
    details: { type: Object                 }, // 商品详细信息
    desc:    { type: String, default: ""    }, // 商品描述
    sets:    { type: String, default: ""    }, // 扩展字段1
    other:   { type: Object                 }, // 扩展字段2
    created: { type: Date, default: Date.now }, //
    updated: { type: Date, default: Date.now },
    status:  { type: String, default: "1" },   // 状态有效1
});

// 日志
var Log = new Schema({
    name:    { type: String},  // 模块名称
    content: { type: Object},   // 日志内容
    msg:     { type: String},
    created: { type: Date, default: Date.now }
});

// 订单
var Order = new Schema({
    userid:   { type: String, required: true }, // 关联的用户
    addr:     { type: Object                 }, // 收货地址
    type:     { type: String,    default: "0"}, // 0 源码 1 书籍  2商品
    orderid:  { type: String, required: true, unique: true }, // 订单号号
    pid:      { type: String, required: true }, // 商品id
    payAmount:{ type: Number, required: true }, // 订单金额
    post:     { type: Object                 }, // 商品
    express:  { type: String                 }, // 快递物流
    score:    { type: Number, default: 0     }, //积分
    tradeno:  { type: String, default: ""    }, // 交易流水号凭证
    outtrade: { type: String, default: ""    }, // 商户订单号
    openAmount:{type: Number, default: 0     }, // 预期收益
    times:    { type: Number, default: 0     }, // 下载次数
    openStatus:{type: String, default: "0"   }, // 0未清算，1已清算
    cStatus:  { type: String, default: "0"    }, // 0未评价，1已评价
    created:  { type: Date,   default: Date.now }, //
    updated:  { type: Date,   default: Date.now },
    status:   { type: String, default: "0" },   // 状态有效， 0未完成订单，1已完成订单, 2赎回中, 3已赎回, 4先息后本
});

// 评论
var Comment = new Schema({
    userid:   { type: String, required: true }, // 关联的用户
    pid:      { type: String, required: true }, // 关联的商品
    header:   { type: String, default: "http://bgn.oss-cn-shanghai.aliyuncs.com/header/cat-01.png!small"}, //头像
    nick:     { type: String, default: ""    }, // 昵称
    orderid:  { type: String, default: ""    }, // 订单ID
    content:  { type: String, default: ""    }, // 评论内容
    score:    { type: Number, default: 0     }, // 评分分数
    reply:    { type: Object                 }, // 回复
    level:    { type: Number, default: 0     }, // 顶赞
    illegal:  { type: Number, default: 0     }, // 非法内容
    topUser:  { type: Array                  }, // 赞用户列表
    illUser:  { type: Array                  }, // 赞非法用户列表
    type:     { type: String, default: "0"   }, // 0为普通评论 1为购买评论
    status:   { type: String, default: "1"   }, // 1显示，0删除
    created:  { type: Date,   default: Date.now }, //
    updated:  { type: Date,   default: Date.now }
});

var Plat = new Schema({
    userid:   { type: String, required: true },   // 关联的用户
    classes:  { type: String, default: "1"   },   // 分类1是建议；2是问题反馈
    title:    { type: String, default: ""      },   // 标题
    content:  { type: String, default: ""    },   // 内容
    anwser:   { type: String, default: ""    },   // 回答
    top:      { type: Number, default: 0     },   // 顶赞
    topUser:  { type: Array                  },   // 赞用户列表
    created:  { type: Date, default: Date.now},
    updated:  { type: Date,   default: Date.now },
    status:   { type: String, default: "1"   },   // 状态有效 1,已操作完整
});
    
var Analyse = new Schema({
    product:  { type: Object}, // 商品统计
    hot:      { type: Object}, // 购买热度统计
    vists:    { type: Object}, // 访问量统计
});




// Bcrypt middleware on UserSchema
User.pre('save', function(next) {
    var user = this;
    if (!user.isModified('password')) return next();
    user.password = md5(user.password);
    next();
});

//Password verification
User.methods.comparePassword = function(password, cb) {
    if(md5(password)==this.password){
        cb(true);
    }else{
        return cb(false);
    }
};


//Define Models
var userModel = mongoose.model('User', User);
var orderModel = mongoose.model('Order', Order);
var logModel = mongoose.model('Log', Log);
var platModel = mongoose.model('Plat', Plat);
var inviteModel = mongoose.model('Invite', Invite);
var postModel = mongoose.model('Post', Post);
var commentModel = mongoose.model('Comment', Comment);
var goldModel = mongoose.model('Gold', Gold);

// Export Models
exports.userModel = userModel;
exports.orderModel = orderModel;
exports.logModel = logModel;
exports.platModel = platModel;
exports.inviteModel = inviteModel;
exports.postModel = postModel;
exports.commentModel = commentModel;
exports.goldModel = goldModel;



