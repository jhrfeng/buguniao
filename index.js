var path = require("path");
var express = require("express");
var jwt = require('express-jwt');
var bodyParser = require('body-parser');
var log4js = require('log4js');
var tokenManager = require('./config/token_manager');
var secret = require('./config/secret');
var logjs = require('./config/log4js');

//Routes
var routes = {};
routes.users = require('./route/user.js');
routes.posts = require('./route/posts.js')
routes.validate = require('./route/validate.js')
routes.aplipay = require('./route/aplipay.js')
routes.order = require('./route/order.js')
routes.invite = require('./route/invite.js')
routes.comment = require('./route/comment.js')
routes.oss = require('./route/oss.js')
routes.home = require('./route/home.js')
routes.plat = require('./route/plat.js')

var app = express();
var serverPort = process.env.PORT || 3000;
app.listen(serverPort, "0.0.0.0", function (err) { // 192.168.7.148
  if (err) {
    console.log(err);
    return;
  }
  console.log("Listening at http://localhost:" + serverPort);
});

log4js.connectLogger(logjs.logger('access'), {level:'info', format:':method :url'})
// app.use(log4js.connectLogger(logjs.logger('access'), {level:'auto', format:':method :url'}));


app.use(function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  // res.header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'html')))

// index.html//默认跳转主页
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

//Logout
app.get('/user/logout', jwt({secret: secret.secretToken}), routes.users.logout); 

//用户登录
app.post('/user/signin', routes.users.signin); 

//创建新用户
app.post('/user/register', routes.users.register); 

//修改用户信息
app.post('/user/setting', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.setting); 

//修改收货地址
app.post('/user/setAddr', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.setAddr); 

//获取用户信息
app.get('/user/me', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.me); 

//源码商品
app.post('/posts/detail', routes.posts.detail); 

//实物商品
app.post('/posts/sdetail', routes.posts.sdetail); 

//查询商品
app.post('/posts/query', routes.posts.queryList); 

//1元商品
app.get('/posts/oneList', routes.posts.oneList); 


//热门商品
app.get('/posts/hotList', routes.posts.hotList); 

//最新商品
app.get('/posts/newList', routes.posts.newList); 

//热门商品
app.get('/posts/shotList', routes.posts.shotList); 

//最新商品
app.get('/posts/snewList', routes.posts.snewList); 

//动态校验码
app.get('/validate/reSend', routes.validate.reSend);

// 短信发送
app.post('/validate/sendsms', routes.validate.sendsms);

// 查看评论
app.get('/comment/query', routes.comment.query);

// 新增评论
app.post('/comment/add', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.comment.add);

// 新增商品评价
app.post('/comment/post', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.comment.post);

//获取用户邀请列表
app.get('/invite/list', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.invite.list); 

//获取用户订单
app.get('/order/mylist', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.mylist); 

//获取用户订单
app.get('/order/myslist', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.myslist); 

//源码下载
app.get('/order/test',  routes.oss.test);
app.get('/order/getfile',  routes.oss.getfile);
app.get('/order/download', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.oss.download);

//获取订单
app.get('/order/detail', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.detail);

//生成订单
app.post('/order/new', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.newOrder);

//生成订单
app.post('/order/snew', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.snewOrder);

//删除订单
app.get('/order/cancelOrder', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.cancelOrder);

//订单确认收费
app.post('/order/hasOk', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.order.hasOk);

//支付宝交易
app.post('/aplipay/pay', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.aplipay.pay);

//支付宝异步通知
app.post('/aplipay/notify', routes.aplipay.notify); 

//统计访问量
app.get('/home/source', routes.home.sources); 

//统计访问量
app.get('/home/getSource', routes.home.getSource); 

//获取首页参数
app.get('/home/getting', routes.home.getting); 

//新增建议
app.post('/plat/addadvice', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.plat.addadvice);

//问题反馈
app.post('/plat/addbug', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.plat.addbug);



process.on('uncaughtException', function(err){
  console.log(err);
})

