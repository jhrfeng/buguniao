var request = require('request');
var logger = require('../config/log4js').logger('access');
let SMS_URL = 'https://sms.yunpian.com/v2/sms/single_send.json';

exports.send = function(mobile, type, content){
	logger.info('semdSMS', mobile, type, content)
	var text = '';
	if(1==type)
		text = '【布谷鸟源码网】手机号为'+content[0]+'的客户已购买'+content[1]+'订单，请您及时处理。'
	else if(2==type)
		text = '【布谷鸟源码网】您的验证码是'+content[0]+'， 本次验证码10分钟内有效。'
	else if(3==type)
		text = '【布谷鸟源码网】我们已收到您购买'+content[0]+'的订单，若不能正常下载，可能是源码版本正在更新，请你稍后1~3个小时后进行下载，感谢您对我们的工作支持！'
	var smsJson = {
				apikey:"966f88af7f6c20b51bb758ffa50c197c",
				text:text,
				mobile:mobile
			};
	request.post({url:SMS_URL, form: smsJson}, function(err,httpResponse,body){
		logger.info('smsJson', err, body)
		// var object = JSON.parse(body);
		// if(object["code"]!=0){}
	});	
}

