var moment = require('moment');
var db = require('../config/mongo_database');
var secret = require('../config/secret');
var redisClient = require('../config/redis_database').redisClient;
var tokenManager = require('../config/token_manager');
var logger = require('../config/log4js').logger('access');

// 详情
exports.detail = function(req, res){
	var pid = req.body.pid || '';
	db.postModel.findOne({status:'1', pid:pid},
	{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1},
	function(err, post){
		return res.json(post);
	})		
}

// 实物详情
exports.sdetail = function(req, res){
	var pid = req.body.pid || '';
	db.postModel.findOne({status:'1', pid:pid},
	{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1,'yprice':1,'shcema':1,'size':1,'issize':1},
	function(err, post){
		return res.json(post);
	})		
}

// 热门
exports.oneList = function(req, res){
	db.postModel.find({status:'1', 'hot':'3', type:'1'}, 
		// {'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1},
		{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tagimg':1,'start':1},
		function(err, list){
		logger.error(err);
		if(err)return res.sendStatus(400);
		return res.json(list);
	})
}

// 热门
exports.hotList = function(req, res){
	db.postModel.find({status:'1', 'hot':'1', type:'1'}, 
		// {'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1},
		{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tagimg':1,'start':1},
		function(err, list){
		logger.error(err);
		if(err)return res.sendStatus(400);
		return res.json(list);
	})
}

// 最新
exports.newList = function(req, res){
	db.postModel.find({status:'1', 'hot':'2', type:'1'}, 
		// {'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1},
		{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tagimg':1,'start':1},
		function(err, list){
		logger.error(err);
		if(err)return res.sendStatus(400);
		return res.json(list);
	})
}

// 实物商品热门
exports.shotList = function(req, res){
	db.postModel.find({status:'1', 'hot':'1', type:'3'}, 
		// {'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1,'yprice':1,'shcema':1,'size':1,'issize':1},
		{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tagimg':1,'start':1},
		function(err, list){
		logger.error(err);
		if(err)return res.sendStatus(400);
		return res.json(list);
	})
}

// 实物商品最新
exports.snewList = function(req, res){
	db.postModel.find({status:'1', 'hot':'0', type:'3'}, 
		// {'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1,'yprice':1,'shcema':1,'size':1,'issize':1},
		{'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tagimg':1,'start':1},
		function(err, list){
		logger.error(err);
		if(err)return res.sendStatus(400);
		return res.json(list);
	})
}

// 查询
exports.queryList = function(req, res){
	var name = req.body.name || '';
	var price = req.body.price || '1';
	var times = req.body.times || '1';
	var created = req.body.created || '-1';
	var type = req.body.type || '';
	var pages = req.body.pages || 0;

	var where = null;
	var where1 = null;
	var where2 = null;
	var sortList = {price:price, times:times, created:created};
	// {$or: [{expires: {$gte: new Date()}}, {expires: null}]}

	logger.info(type)

	if(name!=''){
		where = {tag: new RegExp(name.toLowerCase())} //, $or: [{tag: new RegExp(name)}]
		where1 = {$or: [{name: new RegExp(name.toLowerCase())}]}
	} 

	if(!tokenManager.isEmpty(type)){
		var arr = [];
		for (var name in type){
			arr.push({type1: name})
	    }
		where2 = {$or: arr}
	}
	
	logger.info('queryList:', where,where2,sortList);
	db.postModel.count(where, function(err, count){
		logger.error(err, count)
		db.postModel
		.where({status:'1', type:'1'})
		.where(where)
		.where(where2)
		// .select({'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tag':1,'tagimg':1,'title':1,'demo':1,'details':1,'desc':1,'imgs':1,'start':1})
		.select({'pid':1,'name':1,'codes':1,'oldprice':1,'price':1,'times':1,'tagimg':1,'start':1})
		.sort(sortList)
		.skip(pages*12)
		.limit(12)
		.exec(function(err, list){
			res.json({count:count, list:list});
		})
	});
}


// 转成数据库对象
function toDBObject(type, newOb, oldOb){
	if(type==1){
		newOb.pid =  moment(new Date()).format("YYYYMMDDHHmmss");
		newOb.pid =  newOb.pid + moment(new Date()).format("mmss");
	}
	newOb.name = oldOb.name;
	newOb.codes = oldOb.codes;
	newOb.oldprice = oldOb.oldprice;
	newOb.price = oldOb.price;
	newOb.type = oldOb.type;
	newOb.type1 = oldOb.type1;
	newOb.tag = oldOb.tag;
	newOb.hot = oldOb.hot;
	newOb.version = oldOb.version;
	newOb.demo = oldOb.demo;
	newOb.download = oldOb.download;
	newOb.credate = oldOb.credate;
	newOb.modate = oldOb.modate;
	newOb.title = oldOb.title;
	oldOb.imgs = oldOb.imgs.replace(/\n/g, "");
	newOb.imgs = oldOb.imgs.split(";");
	newOb.codeyuan = oldOb.codeyuan;
	newOb.postlog = oldOb.postlog;
	newOb.details = JSON.parse(oldOb.details);
	newOb.desc = oldOb.desc;
}