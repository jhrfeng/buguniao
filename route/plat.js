var db = require('../config/mongo_database');
var tokenManager = require('../config/token_manager');
var logger = require('../config/log4js').logger('access');


// 添加意见
exports.addadvice = function(req, res){
	var userid = tokenManager.getUserId(req);
	var content = req.body.content || '';  
	if(''==content || ''==content.trim())  
		return res.sendStatus(403);    

	var plat = new db.platModel();
	plat.content = content;
	plat.userid = userid;
	plat.save(function(err) {
		logger.error("addAdvice[platModel]: ", err)
		return res.sendStatus(200);    
	})
}

// 添加问题反馈
exports.addbug = function(req, res){
	var userid  = tokenManager.getUserId(req);
	var title   = req.body.title || '';  
	var content = req.body.content || '';  
	if(''==content || ''==content.trim()
		||''==title || ''==title.trim())  
		return res.sendStatus(403);    

	var plat = new db.platModel();
	plat.title = title;
	plat.content = content;
	plat.classes = '2';
	plat.userid = userid;
	plat.save(function(err) {
		logger.error("addbug[platModel]: ", err)
		return res.sendStatus(200);    
	})
}

// 获取招聘信息
exports.zhaolist = function(req, res){
	db.platModel.find({classes:'3'}, function (err, list) {
		return res.json(list);
	}).sort({updated : -1 })
}
