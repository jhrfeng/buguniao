var db = require('../config/mongo_database');
var moment = require('moment');
var tokenManager = require('../config/token_manager');

// 列表
exports.list = function(req, res) {
	var user = tokenManager.getUser(req);
	db.inviteModel.find({inviteid: user.username}, function (err, result) {
		res.json(result);
	})
};
