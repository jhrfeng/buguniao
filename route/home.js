var redisClient = require('../config/redis_database').redisClient;
var moment = require('moment');
var tokenManager = require('../config/token_manager');

// 获取访问量
exports.getSource = function(req, res) {
	var params = req.query;
	var today = moment(new Date()).format("YYYYMMDD");
	console.log(today+params.source)
	redisClient.get(today+params.source, function (err, rank) {
		console.log(rank)
		res.json(rank);
	})
}


// 统计访问量
exports.sources = function(req, res) {
	var params = req.query;
	console.log(params)
	var today = moment(new Date()).format("YYYYMMDD");
	add(today+params.source);
	res.sendStatus(200);
	
}

// 获取首页参数
exports.getting = function(req, res){
	redisClient.get('home', function (err, home) {
		res.json(JSON.parse(home));
	})
}

function add(today){
	redisClient.get(today, function (err, rank) {
		console.log(today, rank)
		if(rank==null){
			redisClient.set(today,0);
		}
		redisClient.incrby(today, 1);
	})
}