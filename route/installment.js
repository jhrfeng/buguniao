var request = require('request');
var db = require('../config/mongo_database');
var secret = require('../config/secret');
let SMS_URL = 'https://sms.yunpian.com/v2/sms/single_send.json';
var tokenManager = require('../config/token_manager');


/**
  1. 添加订单到分期订单中
  2. 更新订单为先息后本
**/
exports.add = function(req, res){
	var user = tokenManager.getUser(req);
	if(user.id!='56064f89ade2f21f36b03136'){
		return res.sendStatus(500); 
	}else{
		var orderid = req.body.orderid || '';
		console.log(orderid)
		var whereData = {orderid:orderid, status:"1"};
		db.orderModel.findOne(whereData, function(err, order){
			if(order){
				updateOrderStatus(orderid, "4");
				addOrder(order, user);
				res.sendStatus(200); 
			}else{
				res.sendStatus(402); //未查到
			}
		})
	}
	

}

/**
  获取所有订单
**/
exports.query = function(req, res){
	console.log("............")
	db.installmentModel.find({status:"1"},function(err, order){
		res.json({status:200, order:order});
	})
}

/**
  更新所有订单
**/
exports.update = function(req, res){
	var userid = tokenManager.getUserId(req);
	if(userid!='56064f89ade2f21f36b03136'){
		return res.sendStatus(500); 
	}else{
		var updateDat = req.body.opretion;
		var orderid = req.body.orderid;
		var log = new db.logModel();
	    log.name = "分期"+orderid;
		log.content = updateDat;
		db.installmentModel.update({orderid:orderid}, {$set:updateDat}, function(err, order){
			if(order){
				log.save(function(err) {})
				res.sendStatus(200); 
			}
			res.sendStatus(402); //更新失败
		})
	}
	
}


function updateOrderStatus(orderid, status){
	var updateDat = {$set: {status:status, updated:new Date()}}; //如果不用$set，替换整条数据
	db.orderModel.update({orderid:orderid}, updateDat, function(err, uporder){ // 执行订单状态变更
	})
}


// 生成一笔分期单
function addOrder(order, user){
	var opeArry = {account:"", paytype:"", tradeno:"", openStatus:"0", findate:null};
	var installment = new db.installmentModel();

	installment.userid  = order.userid;
	// installment.user = user;
	installment.orderid = order.orderid;
	installment.product = order.pid.name;
	installment.payAmount = order.payAmount;
	installment.openAmount = order.openAmount;
	installment.capital = opeArry;
	var addArry = [];
	if(order.pid.week==30){
		addArry.push(opeArry)
		installment.opretion = addArry;
	}else if(order.pid.week==90){
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		installment.opretion = addArry;
	}else if(order.pid.week==180){
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		installment.opretion = addArry;
	}else if(order.pid.week==360){
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		addArry.push(opeArry);
		installment.opretion = addArry;
	}
	// 进行保存
	installment.save(function(err) {

	});
	
}
